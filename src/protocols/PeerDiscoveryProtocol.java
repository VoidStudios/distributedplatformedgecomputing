package protocols;

import application.Application;
import common.Message;
import common.Node;
import helpers.Constants;
import helpers.ProtocolHelper;
import services.PeerDiscoveryThread;

public class PeerDiscoveryProtocol {

	public static void digest(Message message, Node node) {
		if (message.type.equals("request")) {
			Application.readWriteLock.readLock().lock(); // acquire lock for thread safety
			// prepare node list
			int i = 0;
			String my_nodes[] = new String[Application.known_nodes.values().size()];
			for (Node n : Application.known_nodes.values()) {
				if (n.isHandshake()) {
					my_nodes[i] = n.getAddress() + "," + n.getPort() + "," + n.getId();
					i++;
				}
			}
			Application.readWriteLock.readLock().unlock();
			Message m = new Message("addr", ProtocolHelper.gson.toJson(my_nodes), "response");
			node.sendMessage(m);
			System.out.println(Constants.PROTOCOL + "Sending requested node list to " + node.getId());
		} else if (message.type.equals("response")) {
			String[] potencial_nodes = ProtocolHelper.gson.fromJson(message.content, String[].class);
			System.out.println(Constants.PROTOCOL + "Received " + potencial_nodes.length + " potencial new nodes");
			for (int i = 0; i < potencial_nodes.length; i++) {
				Application.executor.execute(new PeerDiscoveryThread(potencial_nodes[i]));
			}
		}
	}

	public static void requestAddr(Node n) {
		Message m = new Message("addr", "", "request");
		n.sendMessage(m);
		System.out.println(Constants.PROTOCOL + "Requesting new nodes from " + n.getId());
	}
}
