package helpers;
public class Constants {
	public static final String RESET = "\u001B[0m";
	public static final String ERROR = "\u001B[31m Error: \u001B[0m ";
	public static final String SERVER = "\u001B[32m Server: \u001B[0m ";
	public static final String COMM = "\u001B[33m COMM: \u001B[0m ";
	public static final String MISC = "\u001B[34m Note: \u001B[0m";
	public static final String PROTOCOL = "\u001B[35m Protocol \u001B[0m ";
	public static final String DISCOVERY = "\u001B[36m Network \u001B[0m ";
	public static final String WHITE = "\u001B[37m ";
	
}
