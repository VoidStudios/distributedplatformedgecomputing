package services;

import application.Application;
import common.Node;
import helpers.Constants;

public class PeerDiscoveryThread extends Thread {
	public String parameters[];

	public PeerDiscoveryThread(String potencial_node) {
		this.parameters = potencial_node.split(",");
	}

	public void run() {
		Application.readWriteLock.writeLock().lock();
		String ip = parameters[0];
		int port = Integer.parseInt(parameters[1]);
		String id = parameters[2];
		if(!Application.known_nodes.containsKey(id)) { //unknown node
			System.out.println(Constants.DISCOVERY + " establishing new connection with "  +id + " and ip " + ip + " port " +port);
			Node n = new Node(ip, port, id);
			if(n.connect()) {
				System.out.println(Constants.DISCOVERY + "connection success! " + id);
				Application.executor.execute(n);
			}else {
				System.out.println(Constants.DISCOVERY + "connection faiiled! " + id + " reason: " + n.toString());
			}
			
		}else {
			System.out.println(Constants.DISCOVERY + " node "  +id + " already connected..");
		}
		Application.readWriteLock.writeLock().unlock();
	}
}
